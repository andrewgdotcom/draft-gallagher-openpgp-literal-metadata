---
title: "OpenPGP Literal Data Metadata Integrity"
category: info
docname: draft-gallagher-openpgp-literal-metadata-01
updates: 9580
ipr: trust200902
area: int
workgroup: openpgp
keyword: Internet-Draft
submissionType: IETF
venue:
  group: "OpenPGP"
  type: "Working Group"
  mail: "openpgp@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/openpgp/"
  repo: "https://gitlab.com/andrewgdotcom/draft-gallagher-openpgp-literal-metadata"
  latest: "https://andrewgdotcom.gitlab.io/draft-gallagher-openpgp-literal-metadata"
stand_alone: yes
pi: [toc, sortrefs, symrefs]
author:
 -
    fullname: Andrew Gallagher
    organization: PGPKeys.EU
    email: andrewg@andrewg.com
    role: editor
normative:
  FIPS180:
    target: http://dx.doi.org/10.6028/NIST.FIPS.180-4
    title: Secure Hash Standard (SHS), FIPS 180-4
    author:
      org: National Institute of Standards and Technology, U.S. Department of Commerce
    date: August 2015
  RFC9580:
informative:
  RFC1991:
  I-D.koch-librepgp:

--- abstract

This document specifies a method for ensuring the integrity of file metadata when signed using OpenPGP.

--- middle

# Introduction {#introduction}

The PGP literal data packet's metadata fields (file type, file name, and file timestamp) are not covered by any integrity-protection mechanisms and are therefore malleable by an attacker.
[RFC1991] states that this was intentional, to ensure that attached and detached signatures are calculated identically and can be transformed into one another.
Metadata malleability has therefore persisted through subsequent OpenPGP specifications, up to and including [RFC9580].
Although reliance on this metadata is now deprecated, legacy applications may wish to upgrade their cryptographic layer without altering their existing workflow.
This document introduces the missing integrity check by adopting and extending the "Literal Data Meta Hash" subpacket from {{Section 5.2.3.33 of I-D.koch-librepgp}}.

# Conventions and Definitions {#conventions-definitions}

{::boilerplate bcp14-tagged}

# Literal Data Meta subpacket {#literal-data-meta-subpacket}

This subpacket MAY be used to protect the metadata of a Literal Data Packet.
It is only useful when located in the hashed-subpackets area of a v4 (or later) signature over a Literal Data Packet, and so it SHOULD NOT appear elsewhere.
The subpacket SHOULD be marked as critical.

The metadata is always of the same form as it appears in the Literal Data Packet ({{Section 5.9 of RFC9580}}), i.e.:

- A one-octet field that describes how the data is formatted.
- File name as a string (one-octet length, followed by a file name).
- A four-octet number that indicates a date associated with the literal data.

The first octet of the subpacket contents indicates the encoding of the subpacket.
A value of 0 indicates Hashed encoding, and a value of 1 indicates Verbatim encoding.
The remaining octets of the subpacket are encoding-dependent.

## Literal Data Meta (Hashed) {#literal-data-meta-hashed}

The remainder of the packet contains a 32 octet fixed-length hash value.
The hash is computed over the metadata as specified above, using SHA-256 [FIPS180].
When an implementation is validating a signature containing a Literal Data Meta (Hashed) subpacket in its hashed-subpackets area, it MUST re-create the hash from the metadata section of the Literal Data packet that is signed over.
If the calculated hash value does not match the one in the subpacket, the signature MUST be deemed as invalid.

## Literal Data Meta (Verbatim) {#literal-data-meta-verbatim}

The remainder of the packet contains a verbatim copy of the metadata as specified above.
When an implementation has successfully validated a signature containing a Literal Data Meta (Verbatim) subpacket in its hashed-subpackets area, the metadata section of the Literal Data Packet that is signed over MUST be ignored, and the copy from the Literal Data Meta subpacket MAY be used instead.

When a Literal Data Meta (Verbatim) subpacket is present, the metadata section of the corresponding Literal Data packet MUST be empty, i.e. it MUST consist of exactly six octets of zeros.

# Security Considerations {#security-considerations}

A signature containing a Literal Data Meta (Verbatim) subpacket can be round-tripped via detached-signature format without loss of integrity.
A signature containing a Literal Data Meta (Hashed) subpacket does not have this property, because the metadata cannot be regenerated from the hash and is therefore lost on conversion to a detached signature.

It is therefore RECOMMENDED that implementations generate Literal Data Meta subpackets using the Verbatim encoding.

# IANA Considerations {#iana-considerations}

This document requests that the following entry be added to the OpenPGP Signature Subpacket registry:

{: title="OpenPGP Signature Subpacket Registry" #signature-subpacket-registry}
Type | Name               | Specification
-----|--------------------|-------------------------
40   | Literal Data Meta  | This document

--- back

# Acknowledgments {#acknowledgements}

The author would like to thank Werner Koch for his earlier work on the Literal Data Meta Hash subpacket.

# Document History

Note to RFC Editor: this section should be removed before publication.

## Changes Between -00 and -01

* More precise language: "must be replaced by ..." is now "must be ignored and ... MAY be used instead".
* When verbatim is used, literal data fields MUST now be zeroed.
* Updated references.
* Updated preamble to more accurately reflect history.
* Fixed several typos.

# OpenPGP Literal Data Metadata Integrity

This repository holds the source code for the [https://datatracker.ietf.org/doc/draft-gallagher-openpgp-literal-metadata/](OpenPGP Literal Data Metadata Integrity Internet Draft).
Merge requests are welcome.

## List of documents

Included in this repository are the following documents:

* draft-gallagher-openpgp-literal-metadata.md
    Markdown source for the OpenPGP Literal Data Metadata Integrity Internet Draft.
